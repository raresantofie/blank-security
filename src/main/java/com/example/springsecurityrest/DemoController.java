package com.example.springsecurityrest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.PersistenceContext;

@RestController
public class DemoController {

    @PostMapping(value = "/demo")
    public String auth(@RequestBody String auth) {
        return auth;
    }

    @GetMapping(value = "/demo")
    public String get() {
        return "GET";
    }
}