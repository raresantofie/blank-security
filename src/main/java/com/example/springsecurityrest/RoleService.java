package com.example.springsecurityrest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public Role save(Role role) {
        Role roleInDatabase = roleRepository.findRoleByName(role.getName());
        if (roleInDatabase != null) {
            throw new RuntimeException("Role already exists");
        } else {
            Role save = roleRepository.save(role);
            return save;
            // return roleRepository.save(role);
        }
    }

    public Role findByName(RoleTypes roleTypes) {
        return roleRepository.findRoleByName(roleTypes);
    }

}
