package com.example.springsecurityrest;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// JpaRepository<TipulClasei, TipulCheiiPrimareAClasei>
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
